JAM.VehicleFinance = {}
local JVF = JAM.VehicleFinance
JVF.ESX = JAM.ESX

JVF.FinanceKey = "K"
JVF.FinanceStartingAmount = 10
JVF.MaxRepayTime = 48 -- hours
JVF.TowDriverJob = "cardealer"
JVF.RepoPoint = vector3(-44.28, -1083.45, 25.50)
JVF.MaxRepayments = 10