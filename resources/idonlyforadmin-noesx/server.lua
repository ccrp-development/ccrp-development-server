--[[

  Made with love by Cheleber, you can join my RP Server here: www.grandtheftlusitan.com
  Just add your admins steam id to server and client!

--]]

--- ADD YOUR STEAM ID OR LICENSE FROM YOUR ADMINS!
local admins = {
	'steam:1100001124bee3e',
	'steam:1100001076e0d0c',
	'steam:11000010344b88c',
	'steam:1100001025fc0aa',
	'steam:110000104e6e87e',
	'steam:11000010a58735e',
	'steam:110000106619bd0',
	'steam:11000011210b034',
	'steam:110000110492904',
	'steam:11000010e7390b5',
	'steam:110000104bf4e6c',
	'steam:110000105fc6d37',
	'steam:1100001101c8ee4',
	'steam:1100001063900e5',
	'steam:1100001041b7de1',
	'steam:11000011b948ce9',
	'steam:110000106eaa321',
	'steam:11000010ea6bc61',
	'steam:11000010ddb9a52',
	'steam:1100001095968b9',
	'steam:1100001099ce2ca',
}


function isAdmin(player)
    local allowed = false
    for i,id in ipairs(admins) do
        for x,pid in ipairs(GetPlayerIdentifiers(player)) do
            if string.lower(pid) == string.lower(id) then
                allowed = true
            end
        end
    end
    return allowed
end



AddEventHandler('chatMessage', function(source, color, msg)
	cm = stringsplit(msg, " ")
	if cm[1] == "/noid" then
		CancelEvent()
		if isAdmin(source) then
		    TriggerClientEvent("dontshowid", -1, source)
		    TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID's OFF!")
		end
	end	
	if cm[1] == "/seeid" then
		CancelEvent()
		if isAdmin(source) then
		    TriggerClientEvent("showid", -1, source)
		    TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID's ON!")
		end
	end	
end)

RegisterServerEvent('checkadmin')
AddEventHandler('checkadmin', function()
	local id = source
	if isAdmin(id) then
		TriggerClientEvent("setgroup", source)
	end
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end
